import React, { useEffect } from "react";

import routeMain from "./routes";

import TitleItem from "components/TitleItem";
import NewsList from "components/NewsList";

import { useDispatch, useSelector } from "react-redux";
import { loadNews } from "store/news/actions";
import { selectList } from "store/news/selectors";
import { TypedDispatch } from "store";

import './styles.scss';

const NewsListPage = () => {
    const dispatch = useDispatch<TypedDispatch>();
    const newsList = useSelector(selectList);

    useEffect(() => {
        dispatch(loadNews());
    }, [dispatch])

    return (
        <section className="newsListPageWrapper">
            <div className="newsListPage">
                <TitleItem
                    title={
                        <h2>
                            Быть <br/> в курсе <span>событий</span>
                        </h2>
                    }
                />
                {newsList.length > 0 && <NewsList list={newsList}/>}
            </div>
        </section>
    )
}

export {routeMain};

export default NewsListPage;