import React, {useState, useEffect} from "react";
import {useParams} from "react-router-dom";

import { useSelector } from "react-redux";
import { selectList } from "store/news/selectors";

import routeMain from "./routes";

import DateView from "components/DateView";

import {ID} from 'types/ID';
import {INewsDetail} from "types/INewsDetail";

import './styles.scss';

const NewsDetailPage = () => {
    const {id} = useParams<ID>();
    const [news, setNews] = useState<INewsDetail | undefined>(undefined);
    
    const newsList = useSelector(selectList);

    useEffect(() => {
        const carrentNews = newsList?.find(
            (item: INewsDetail) => item._id === id
        )
        setNews(carrentNews);
    }, [id, newsList]);

    return (
        <div className="detailPageWrapper">
            {news ? (
                <div className="detailPage">
                    <div className="title">
                        <h2 className="titleText">{news.title}</h2>
                        <div className="titleAttributes">
                            <p className="titleSource">{news.clean_url}</p>
                            <DateView value={news.published_date}/>
                        </div>
                    </div>
                    <div className="description">
                        <img className="descriptionImage" src={news.media} alt={news.media}/>
                        <p className="descriptionText">{news.summary}</p>
                    </div>
                </div>
            ) : <></>}
        </div>
    )
}

export {routeMain};

export default NewsDetailPage;