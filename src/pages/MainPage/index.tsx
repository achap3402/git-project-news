import React, {useEffect} from "react";
import routeMain from "./routes";

import { useDispatch, useSelector } from "react-redux";
import { loadNews } from "store/news/actions";
import { selectList } from "store/news/selectors";
import { TypedDispatch } from "store";

import TitleItem from "components/TitleItem";

import './styles.scss';

const MainPage = () => {
    const dispatch = useDispatch<TypedDispatch>();
    const newsList = useSelector(selectList);

    useEffect(() => {
        dispatch(loadNews());
    }, [dispatch])

    return (
        <section className="mainPageWrapper">
            <div className="mainPage">
                <TitleItem
                    title={
                        <h2>
                            Всегда <br/> свежие <span>новости</span>
                            <p>the latest news</p>
                        </h2>
                    }
                />
            </div>
        </section>
    )
}

export {routeMain};
export default MainPage;