import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";

import MainPage, {routeMain as routeMainPage} from "pages/MainPage";
import NewsListPage, {routeMain as routeNewsListPage} from "pages/NewsListPage";
import NewsDetailPage, {routeMain as routeNewsDetailPage} from "pages/NewsDetailPage";

import Header from "components/Header";
import Footer from "components/Footer";

import './styles.scss';

const AppContent = () => {
  return (
    <div className="appWrapper">
      <Header/>
      <main>
        <Switch>
          <Route exact path={routeMainPage()} component={MainPage}/>
          <Route exact path={routeNewsListPage()} component={NewsListPage}/>
          <Route exact path={routeNewsDetailPage()} component={NewsDetailPage}/>
          <Redirect
            to={{
              pathname: routeMainPage()
            }}
          />
        </Switch>
      </main>
      <Footer/>
    </div>
  );
}

export default AppContent;
