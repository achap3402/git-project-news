import React, { useState } from "react";
import { NavLink } from "react-router-dom";

import {routeMain as routeMainPage} from "pages/MainPage";
import {routeMain as routeNewsListPage} from "pages/NewsListPage";

import './styles.scss';

const Header = () => {
  const [active, setActive] = useState(false);

  const toggle = () => {
    setActive(!active);
  }

  return (
    <header className="header">
      <div className="mainHeader">
        <div className="title">N E W S</div>
        <div className={!active ? 'header-burger' : 'header-burger header-burger--active'} onClick={() => toggle()}>
          <span></span>
        </div>
        <nav className={!active ? 'header-menu' : 'header-menu header-menu--active'}>
          <NavLink to={routeMainPage()} onClick={() => toggle()} activeClassName={'linkActive'}>
            Главная
          </NavLink>
          <NavLink to={routeNewsListPage()} onClick={() => toggle()} activeClassName={'linkActive'}>
            Новости
          </NavLink>
        </nav>
      </div>
    </header>
  );
}

export default Header;
