import React from "react";

import './styles.scss';

interface IPageTitleParams {
    title: JSX.Element;
}

const TitleItem: React.FC<IPageTitleParams> = ({title}) =>  {
    return (
        <div className="titleItem">{title}</div>
    )    
}

export default TitleItem;