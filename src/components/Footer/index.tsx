import React from "react";

import './styles.scss';

const Footer = () => {
  return (
    <footer className="mainFooter">
      <div className="footer-logo">
        NEWS
        <div className="small small-left">
          Single Page Application
        </div>
      </div>
      <div className="footer-year">
        &copy; 2 0 2 3
      </div>
      <div className="footer-author">
        <div className="small small-right">
          Made by
        </div>
        Alexey Chaplygin
      </div>
    </footer>
  );
}

export default Footer;
