import axios, {AxiosResponse, AxiosRequestConfig} from "axios";

const getNews = (): Promise<AxiosResponse> => {
  const options: AxiosRequestConfig = {
    method: 'GET',
    url: 'https://free-news.p.rapidapi.com/v1/search',
    params: {q: 'Elon Musk', lang: 'en'},
    headers: {
      'X-RapidAPI-Key': 'd6011c0b6fmshc971a2231639c97p13b709jsn770745cb7e96',
      'X-RapidAPI-Host': 'free-news.p.rapidapi.com'
    }
  };
  
  return axios.request(options)
}

export default getNews;