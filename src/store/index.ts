import { legacy_createStore as createStore, combineReducers, applyMiddleware, AnyAction } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk, { ThunkDispatch } from "redux-thunk";

import newsReducer from "store/news/reducer";

const rootReducer = combineReducers({
    newsReducer,
})

const store = createStore(
    rootReducer, composeWithDevTools(applyMiddleware(thunk))
)

export type ReduxState = ReturnType<typeof rootReducer>;
export type TypedDispatch = ThunkDispatch<ReduxState, any, AnyAction>;

export default store;